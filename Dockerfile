FROM python:3
ENV PYTHONUNBUFFERED 1

RUN apt-get update -y && \
    apt-get dist-upgrade -y

RUN mkdir /code && \
    mkdir -p /srv/packages && \
    mkdir -p /secrets

WORKDIR /code
COPY . /code/

RUN pip install -r requirements.txt

# VOLUME /srv/packages

RUN chown :0 /srv
RUN chmod -R g=u /srv
RUN chown :0 /secrets
RUN chmod -R g=u /secrets

EXPOSE 8080

CMD ["/code/start.sh"]
