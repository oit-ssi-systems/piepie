#!/usr/bin/env bash

# python3 ./setup_credentials.py

pypi-server \
    -p 8080 \
    -P /auth/auth.htpasswd \
    /srv/packages
