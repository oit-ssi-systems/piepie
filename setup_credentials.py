#!/usr/bin/env python3
import sys
import hvac
import os
import htpasswd

HTPASSWD_FILE = os.environ.get("HTPASSWD", '/secrets/.htpasswd.txt')
if not HTPASSWD_FILE:
    sys.stderr.write("No HTPASSWD set\n")
    sys.exit(2)


def touch(fname):
    if not os.path.exists(fname):
        open(fname, 'a').close()


def main():

    # Vault connection
    vault = hvac.Client(url=os.environ.get('VAULT_ADDR'))
    if 'VAULT_TOKEN' in os.environ:
        sys.stderr.write("Using VAULT_TOKEN auth\n")
        vault.token = os.environ['VAULT_TOKEN']
    elif 'VAULT_OKD' in os.environ:
        sys.stderr.write("Using K8s auth")
        f = open('/var/run/secrets/kubernetes.io/serviceaccount/token')
        jwt = f.read()
        vault.auth_kubernetes("piepie",
                              jwt,
                              mount_point=os.environ.get('VAULT_OKD'))

    # Create if not exists
    touch(HTPASSWD_FILE)

    # Loops through credentials
    okd_route = os.environ.get('OKD_ROUTE')
    if not okd_route:
        sys.stderr.write("OKD_ROUTE is required\n")
        sys.exit(2)

    vault_path = "secret/piepie/%s" % (okd_route)

    for cred in vault.list(vault_path)['data']['keys']:

        cred_info = vault.read('%s/%s' % (vault_path, cred))['data']
        with htpasswd.Basic(HTPASSWD_FILE) as userdb:

            # Create user if not existy
            if cred_info['username'] not in userdb.users:
                print("Adding %s" % cred_info['username'])
                userdb.add(cred_info['username'], cred_info['password'])

            # Always set password, in case it changed
            print("Setting password for %s" % cred_info['username'])
            userdb.change_password(cred_info['username'],
                                   cred_info['password'])
    return 0


if __name__ == "__main__":
    sys.exit(main())
