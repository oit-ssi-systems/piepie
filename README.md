# Duke PiePie

This is an OKD deployment of
[pypiserver](https://github.com/pypiserver/pypiserver).

## Using this repo

Set an environment variable for pip to use:

```
export PIP_EXTRA_INDEX_URL=https://piepie.oit.duke.edu/simple/
```

windows/powershell: 
```
$env:PIP_EXTRA_INDEX_URL="https://piepie.oit.duke.edu/simple/"
```

## Getting Packages In

We are just using the NFS share in the container here, so use something like:

``` 
$ oc rsync dist/ piepie-NN-XXXX:/srv/packages/
```

Make sure you have built a tar.gz of the package using something like:

```
$ python3 ./setup.py sdist --formats=gztar,zip
```
